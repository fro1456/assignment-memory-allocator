//
// Created by alex-rs001 on 12/30/21.
//
#include "tests.h"
#include "mem.h"
#include "mem_internals.h"
#include "util.h"


//test 1 - memory allocation
void test_1(){
    printf("--------TEST-1 MEMORY ALLOCATION--------\n");
    void* debug_data  = heap_init(10000);
    printf("Heap Info before allocaton\n");
    debug_heap(stdout, debug_data);

    printf("starting allocating memory...\n");

    _malloc(600);
    printf("Heap Info after allocaton\n");
    debug_heap(stdout, debug_data);

    printf("TEST 1 DONE!\n");
}
void test_2(){

    printf("--------TEST-2 FREE BLOCKS--------\n");
    void* debug_data = heap_init(10000);

    void* block = _malloc(100);

    _malloc(500);

    printf("Heap before clearing: \n");
    debug_heap(stdout, debug_data);

    _free(block);

    printf("Heap after clearing: \n");
    debug_heap(stdout, debug_data);

    printf("TEST 2 DONE!\n");

}
void test_3(){
    printf("--------TEST-3 FREE MANY BLOCKS--------\n");
    void* debug_data = heap_init(10000);

    void* b1 = _malloc(5000);
    void* b2 = _malloc(3000);
    _malloc(500);


    printf("Before clearing\n");
    debug_heap(stdout, debug_data);

    printf("After clearing first\n");
    _free(b1);
    debug_heap(stdout, debug_data);

    printf("After clearing second\n");
    _free(b2);
    debug_heap(stdout, debug_data);


    printf("TEST 3 DONE!\n");

}

void test_4(){
    printf("--------TEST-4 MEMORY TESTING--------\n");
    void* debug_data = heap_init(10000);


    _malloc(1000);
    printf("allocating first block...\n");
    debug_heap(stdout, debug_data);

    printf("NOW WE NEED MORE MEMORY FOR THE SECOND BLOCK\n");
    _malloc(20000);
    printf("allocating second block...\n");
    debug_heap(stdout, debug_data);
    printf("TEST 4 DONE!\n");
}
void test_5(){
    printf("--------TEST-5 MEMORY ENDED--------\n");
    void* debug_data = heap_init(10000);
    printf("original heap");
    debug_heap(stdout,debug_data);


    struct block_header *last = debug_data;
    while (last->next){
        last = last->next;
    }



    printf("Allocating new very huge block of memory... \n");
    void* a =mmap(last->contents + last->capacity.bytes, 10000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, 0, 0);
    (void) a;

    _malloc(50000);
    printf("Final state of memory \n");
    debug_heap(stdout, debug_data);
    printf("TEST 5 DONE!\n");
}
